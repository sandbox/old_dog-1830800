<?php

/**
 * Collection of functions for parsing a gedcom file.
 */
ini_set('auto_detect_line_endings',true);

class Gedcom {
 function parseGedcom($file,$file_offset,$start_time,$max_time) {
   $this->indi_recs = 0;
   $this->fam_recs = 0;
   $this->handle = fopen($file, "r");
   if (!$this->handle) { return false; }
   else {
    fseek($this->handle,$file_offset);
    while (!feof($this->handle)) {
      $gedcom_offset = ftell($this->handle);
      if ($max_time > 0) {
        if ((microtime_float() - $start_time) > $max_time) {
          fclose ($this->handle);
          return array($gedcom_offset, $this->indi_recs, $this->fam_recs);
        }
      }
      $line = trim(fgets($this->handle));
        if (preg_match('/^0 @(\S+)@ INDI/', $line)){
          if (!$this->parseIndi($line)) {return false;}
        }
        else if (preg_match('/^0 @(\S+)@ FAM/', $line)){
          if (!$this->parseFam($line)) {return false;}
        }
        else if (preg_match('/^0 TRLR/', $line)){
          fclose ($this->handle);
          return array(0, $this->indi_recs, $this->fam_recs);
        }
      }
    }
    fclose ($this->handle);
    return 0;
  }
  function parseIndi($line) {
		$indi = array();
		$indi_event = array();
		preg_match('/^0 @I(\S+)@ INDI/', $line, $match);
		$indi['id'] = $match[1];
		while (!feof($this->handle)) {
			$gedcom_offset = ftell($this->handle);
			$line = trim(fgets($this->handle));
			$level = $this->getLevel($line);
			if ($level === 0) {
				fseek($this->handle,$gedcom_offset);
				$ind_values['indid'] = isset($indi['id']) ? $indi['id'] : '';
        $ind_values['fullname'] = $indi['givn'].' '.$indi['surn'];
        $ind_values['givenname'] = $indi['givn'];
        $ind_values['surname'] = $indi['surn'];
				$ind_values['gender'] = isset($indi['sex']) ? $indi['sex'] : '';
				$ind_values['birthdate'] = isset($indi['birt']) ? $indi['birt'] : '';
				$ind_values['birthplace'] = isset($indi['birt_plac']) ? $indi['birt_plac'] : '';
				$ind_values['deathdate'] = isset($indi['deat']) ? $indi['deat'] : '';
				$ind_values['deathplace'] = isset($indi['deat_plac']) ? $indi['deat_plac'] : '';
				$ind_values['fams'] = isset($indi['fams']) ? implode('+',$indi['fams']) : '';
        $ind_values['famc'] = isset($indi['famc']) ? $indi['famc'] : NULL;
        $ind_values['lastchange'] = isset($indi['chan']) ? $indi['chan'] : '';
        $ind_entity = entity_create('ftree_individual', $ind_values);
        $ind_entity->save();
        $this->indi_recs++;
				return true;
			}
			else if (preg_match('/^1 SEX (.*)/', $line, $match)){
				$indi['sex'] = $match[1];
			}
			else if (preg_match('/^1 NAME/', $line)){
				$name = $this->parseName($line);
				if ($name === false) {return false;}
				$indi['givn'] = $name['givn'];
				$indi['surn'] = $name['surn'];
			}
			else if (preg_match('/^1 (BIRT|DEAT)/', $line, $match)){
        $type = $match[1];
				$indi_event = $this->parseEvent($line);
				if ($indi_event === false) {return false;}
				if ($type == 'BIRT') {
          $indi['birt'] = isset($indi_event['date']) ? $indi_event['date'] : '';
          $indi['birt_plac'] = isset($indi_event['plac']) ? $indi_event['plac'] : '';
				}
				if ($type == 'DEAT') {
          $indi['deat'] = isset($indi_event['date']) ? $indi_event['date'] : '';
          $indi['deat_plac'] = isset($indi_event['plac']) ? $indi_event['plac'] : '';
				}
			}
			else if (preg_match('/^1 CHAN/', $line)){
				$indi['chan'] = $this->parseChan($line);
			}
			else if (preg_match('/^1 FAMS @F(\S+)@/', $line, $match)){
				$indi['fams'][] = $match[1];
			}
			else if (preg_match('/^1 FAMC @F(\S+)@/', $line, $match)){
				$indi['famc'] = $match[1];
			}
		}
	//End of parseIndi
	}
  
	function parseFam($line) {
		$fam = array();
		$fam_event = array();
		preg_match('/^0 @F(\S+)@ FAM/',$line,$match);
		$fam['id'] = $match[1];
		while (!feof($this->handle)) {
			$gedcom_offset = ftell($this->handle);
			$line = trim(fgets($this->handle));
			$level = $this->getLevel($line);
			if ($level === 0) {
				fseek($this->handle,$gedcom_offset);
				$fam_values['famid'] = $fam['id'];
				$fam_values['marr_date'] = isset($fam['marr']) ? $fam['marr'] : '';
				$fam_values['marr_plac'] = isset($fam['marr_plac']) ? $fam['marr_plac'] : '';
				$fam_values['div_date'] = isset($fam['div']) ? $fam['div'] : '';
				$fam_values['div_plac'] = isset($fam['div_plac']) ? $fam['div_plac'] : '';
        $fam_values['children'] = isset($fam['child']) ? implode('+', $fam['child']) : '';
        $fam_values['husband'] = isset($fam['husb']) ? $fam['husb'] : 'unknown';
        $fam_values['wife'] = isset($fam['wife']) ? $fam['wife'] : 'unknown';
				$fam_values['lastchange'] = isset($fam['chan']) ? $fam['chan'] : '';
        $fam_entity = entity_create('ftree_family', $fam_values);
        $fam_entity->save();
        $this->fam_recs++;
				return true;
			}
			else if (preg_match('/^1 HUSB @I(\S+)@/', $line, $match)){
				$fam['husb'] = $match[1];
			}
			else if (preg_match('/^1 WIFE @I(\S+)@/', $line, $match)){
				$fam['wife'] = $match[1];
			}
			else if (preg_match('/^1 CHIL @I(\S+)@/', $line, $match)){
				$fam['child'][] = $match[1];
			}
			else if (preg_match('/^1 CHAN/', $line)){
				$fam['chan'] = $this->parseChan($line);
			}

			else if (preg_match('/^1 (DIV|MARR)/', $line, $match)){
        $type = $match[1];
				$fam_event = $this->parseEvent($line);
				if ($fam_event === false) {return false;}
        if ($type == 'MARR'){
          $fam['marr'] = isset($fam_event['date']) ? $fam_event['date'] : '';
          $fam['marr_plac'] = isset($fam_event['plac']) ? $fam_event['plac'] : '';
        }
        if ($type == 'DIV'){
          $fam['div'] = isset($fam_event['date']) ? $fam_event['date'] : '';
          $fam['div_plac'] = isset($fam_event['plac']) ? $fam_event['plac'] : '';
        }
			}
			else if (preg_match('/^1 CHAN/', $line)){
				$fam['chan'] = $this->parseChan($line);
			}
		}
	//End of parseFam
	}
  
	function parseChan($line) {
		$time = '';
		while (!feof($this->handle)) {
			$gedcom_offset = ftell($this->handle);
			$line = trim(fgets($this->handle));
			$level = $this->getLevel($line);
			if ($level <= 1) {
				fseek($this->handle,$gedcom_offset);
        $date = date_create($day.' '.$time);
        return (date_format($date, 'Y-m-d H:i:s'));
      }
			else if (preg_match('/^2 DATE (.*)/', $line, $match)){
				$day = $match[1];
			}
			else if (preg_match('/^3 TIME (.*)/', $line, $match)){
				$time = $match[1];
			}
		}
	//End of parseChan
	}
	
	function parseName($line) {
		$name = array();
    $name['givn'] = '';
    $name['surn'] = '';
		if (preg_match('/^1 NAME (.*)/',$line, $match)) {
			$names = explode('/', trim($match[1]));
      $name['givn'] = $names[0];
      $name['surn'] = isset($names[1]) ? $names[1] : '';
			}
		while (!feof($this->handle)) {
			$gedcom_offset = ftell($this->handle);
			$line = trim(fgets($this->handle));
			$level = $this->getLevel($line);
			if ($level <= 1) {
				fseek($this->handle,$gedcom_offset);
				return $name;
			}
			else if (preg_match('/^2 (GIVN|SURN) (.*)/', $line, $match)){
				$tag = strtolower($match[1]);
				$name[$tag] = $match[2];
			}
		}
	//End of parseName
	}
  
	function parseEvent($line) {
		$event = array();
		while (!feof($this->handle)) {
			$gedcom_offset = ftell($this->handle);
			$line = trim(fgets($this->handle));
			$level = $this->getLevel($line);
			if ($level <= 1) {
				fseek($this->handle,$gedcom_offset);
				return $event;
			}
			else if (preg_match('/^2 PLAC (.*)/', $line, $match)){
				$event['plac'] = $match[1];
			}
			else if (preg_match('/^2 DATE (.*)/', $line, $match)){
				$event['date'] = $match[1];
			}
		}
	//End of parseEvent
	}
  	
	function getLevel($line) {
		$line_array = explode(' ',trim($line),2);
		$level = intval($line_array[0]);
		return $level;
	}

}


