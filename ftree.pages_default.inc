<?php

/**
 * implements hook_default_page_manager_pages
 */
function ftree_default_page_manager_pages(){
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'pedigree';
  $page->task = 'page';
  $page->admin_title = 'pedigree';
  $page->admin_description = '';
  $page->path = 'pedigree/%ped_root';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'ped_root' => array(
      'id' => 1,
      'identifier' => 'root person',
      'name' => 'entity_id:ftree_individual',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_pedigree_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'pedigree';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Pedigree Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'css_id' => 'pedigree',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      0 => array(
        'identifier' => 'Family from root',
        'keyword' => 'root_family',
        'name' => 'entity_from_schema:famc-ftree_individual-ftree_family',
        'context' => 'argument_entity_id:ftree_individual_1',
        'id' => 1,
      ),
      1 => array(
        'identifier' => 'far',
        'keyword' => 'far',
        'name' => 'entity_from_schema:husband-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_1',
        'id' => 1,
      ),
      2 => array(
        'identifier' => 'mor',
        'keyword' => 'mor',
        'name' => 'entity_from_schema:wife-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_1',
        'id' => 1,
      ),
      3 => array(
        'identifier' => 'Family from far',
        'keyword' => 'far_family',
        'name' => 'entity_from_schema:famc-ftree_individual-ftree_family',
        'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_1',
        'id' => 2,
      ),
      4 => array(
        'identifier' => 'Family from mor',
        'keyword' => 'mor_family',
        'name' => 'entity_from_schema:famc-ftree_individual-ftree_family',
        'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_1',
        'id' => 3,
      ),
      5 => array(
        'identifier' => 'farfar',
        'keyword' => 'farfar',
        'name' => 'entity_from_schema:husband-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_2',
        'id' => 2,
      ),
      6 => array(
        'identifier' => 'farmor',
        'keyword' => 'farmor',
        'name' => 'entity_from_schema:wife-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_2',
        'id' => 2,
      ),
      7 => array(
        'identifier' => 'morfar',
        'keyword' => 'morfar',
        'name' => 'entity_from_schema:husband-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_3',
        'id' => 3,
      ),
      8 => array(
        'identifier' => 'mormor',
        'keyword' => 'mormor',
        'name' => 'entity_from_schema:wife-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_3',
        'id' => 3,
      ),
      9 => array(
        'identifier' => 'Family from farfar',
        'keyword' => 'farfar_family',
        'name' => 'entity_from_schema:famc-ftree_individual-ftree_family',
        'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_2',
        'id' => 4,
      ),
      10 => array(
        'identifier' => 'Family from farmor',
        'keyword' => 'farmor_family',
        'name' => 'entity_from_schema:famc-ftree_individual-ftree_family',
        'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_2',
        'id' => 5,
      ),
      11 => array(
        'identifier' => 'Family from morfar',
        'keyword' => 'morfar_family',
        'name' => 'entity_from_schema:famc-ftree_individual-ftree_family',
        'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_3',
        'id' => 6,
      ),
      12 => array(
        'identifier' => 'Family from mormor',
        'keyword' => 'mormor_family',
        'name' => 'entity_from_schema:famc-ftree_individual-ftree_family',
        'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_3',
        'id' => 7,
      ),
      13 => array(
        'identifier' => 'farfars far',
        'keyword' => 'farfars-far',
        'name' => 'entity_from_schema:husband-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_4',
        'id' => 4,
      ),
      14 => array(
        'identifier' => 'farfars mor',
        'keyword' => 'farfars-mor',
        'name' => 'entity_from_schema:wife-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_4',
        'id' => 4,
      ),
      15 => array(
        'identifier' => 'farmors far',
        'keyword' => 'farmors-far',
        'name' => 'entity_from_schema:husband-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_5',
        'id' => 5,
      ),
      16 => array(
        'identifier' => 'farmors mor',
        'keyword' => 'farmors-mor',
        'name' => 'entity_from_schema:wife-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_5',
        'id' => 5,
      ),
      17 => array(
        'identifier' => 'morfars far',
        'keyword' => 'morfars-far',
        'name' => 'entity_from_schema:husband-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_6',
        'id' => 6,
      ),
      18 => array(
        'identifier' => 'morfars mor',
        'keyword' => 'morfars-mor',
        'name' => 'entity_from_schema:wife-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_6',
        'id' => 6,
      ),
      19 => array(
        'identifier' => 'mormors far',
        'keyword' => 'mormors-far',
        'name' => 'entity_from_schema:husband-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_7',
        'id' => 7,
      ),
      20 => array(
        'identifier' => 'mormors mor',
        'keyword' => 'mormors-mor',
        'name' => 'entity_from_schema:wife-ftree_family-ftree_individual',
        'context' => 'relationship_entity_from_schema:famc-ftree_individual-ftree_family_7',
        'id' => 7,
      ),
    ),
    'access' => array(
      'logic' => 'and',
    ),
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
    'middle' => array(
      'style' => '-1',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:ftree_individual_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox0',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $display->content['new-1'] = $pane;
    $display->panels['middle'][0] = 'new-1';
    $pane = new stdClass();
    $pane->pid = 'new-2';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox1',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $display->content['new-2'] = $pane;
    $display->panels['middle'][1] = 'new-2';
    $pane = new stdClass();
    $pane->pid = 'new-3';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox2',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $display->content['new-3'] = $pane;
    $display->panels['middle'][2] = 'new-3';
    $pane = new stdClass();
    $pane->pid = 'new-4';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_2',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_2',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox3',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $display->content['new-4'] = $pane;
    $display->panels['middle'][3] = 'new-4';
    $pane = new stdClass();
    $pane->pid = 'new-5';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_2',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_2',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox4',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $display->content['new-5'] = $pane;
    $display->panels['middle'][4] = 'new-5';
    $pane = new stdClass();
    $pane->pid = 'new-6';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_3',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_3',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox5',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $display->content['new-6'] = $pane;
    $display->panels['middle'][5] = 'new-6';
    $pane = new stdClass();
    $pane->pid = 'new-7';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_3',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_3',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox6',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $display->content['new-7'] = $pane;
    $display->panels['middle'][6] = 'new-7';
    $pane = new stdClass();
    $pane->pid = 'new-8';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_4',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_4',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox7',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $display->content['new-8'] = $pane;
    $display->panels['middle'][7] = 'new-8';
    $pane = new stdClass();
    $pane->pid = 'new-9';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_4',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_4',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox8',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 8;
    $pane->locks = array();
    $display->content['new-9'] = $pane;
    $display->panels['middle'][8] = 'new-9';
    $pane = new stdClass();
    $pane->pid = 'new-10';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_5',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_5',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox9',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 9;
    $pane->locks = array();
    $display->content['new-10'] = $pane;
    $display->panels['middle'][9] = 'new-10';
    $pane = new stdClass();
    $pane->pid = 'new-11';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_5',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_5',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox10',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 10;
    $pane->locks = array();
    $display->content['new-11'] = $pane;
    $display->panels['middle'][10] = 'new-11';
    $pane = new stdClass();
    $pane->pid = 'new-12';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_6',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_6',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox11',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 11;
    $pane->locks = array();
    $display->content['new-12'] = $pane;
    $display->panels['middle'][11] = 'new-12';
    $pane = new stdClass();
    $pane->pid = 'new-13';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_6',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_6',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox12',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 12;
    $pane->locks = array();
    $display->content['new-13'] = $pane;
    $display->panels['middle'][12] = 'new-13';
    $pane = new stdClass();
    $pane->pid = 'new-14';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_7',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:husband-ftree_family-ftree_individual_7',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox13',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 13;
    $pane->locks = array();
    $display->content['new-14'] = $pane;
    $display->panels['middle'][13] = 'new-14';
    $pane = new stdClass();
    $pane->pid = 'new-15';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'individual-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_7',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:wife-ftree_family-ftree_individual_7',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'pedbox14',
      'css_class' => 'pedbox',
    );
    $pane->extras = array();
    $pane->position = 14;
    $pane->locks = array();
    $display->content['new-15'] = $pane;
    $display->panels['middle'][14] = 'new-15';
    $pane = new stdClass();
    $pane->pid = 'new-16';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Grid',
      'title' => '',
      'body' => '<p class="line" id="line0"></p>
  <p class="line" id="line1"></p>
  <p class="line" id="line2"></p>
  <p class="line" id="line3"></p>
  <p class="line" id="line4"></p>
  <p class="line" id="line5"></p>
  <p class="line" id="line6"></p>

  <p class="connector" id="connector1"></p>
  <p class="connector" id="connector2"></p>
  <p class="connector" id="connector3"></p>
  <p class="connector" id="connector4"></p>
  <p class="connector" id="connector5"></p>
  <p class="connector" id="connector6"></p>
  <p class="connector" id="connector7"></p>

  ',
      'format' => 'full_html',
      'substitute' => 0,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 15;
    $pane->locks = array();
    $display->content['new-16'] = $pane;
    $display->panels['middle'][15] = 'new-16';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  
  $pages['pedigree page'] = $page;
  
  return $pages;
}