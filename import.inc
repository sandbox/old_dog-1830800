<?php

/**
 * @file
 * Functions for importing GEDCOM files to database
 */

function microtime_float() {
  list($usec, $sec) = explode(" ", microtime());
  return ((float)$usec + (float)$sec);
}


/**
 * Generate a form for uploading a GEDCOM file
*/
function ftree_import() {
  return drupal_get_form('ftree_import_form');
}

/**
 * Define the file upload form
 */
function ftree_import_form($form, &$form_state) {
 if (!empty($form_state['page_num']) && $form_state['page_num'] == 2){
    return ftree_import_continue_form($form, $form_state);
  }
 if (!empty($form_state['page_num']) && $form_state['page_num'] == 3){
    return ftree_import_done_form($form, $form_state);
  }
  $form_state['gedoffset'] = 0;
  $form_state['irecs'] = 0;
  $form_state['frecs'] = 0;
  $form_state['page_num'] = 1;
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['gedcom_file'] = array(
    '#type' => 'file',
    '#title' => t('GED file to upload'),
    '#size' => 40,
  );
  $form['replace'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace existing GED data'),
    '#default_value' => variable_get('ftree_import_replace', 1),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Start Import'),
    '#validate' => array('ftree_import_validate'),
    '#submit' => array('ftree_import_submit'),
  );
  return $form;
}

function ftree_import_continue_form($form, &$form_state){
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Import Continue after time-out....press Continue'),
    '#markup' => t('Processed '.$form_state['irecs'].' individual records and '.$form_state['frecs'].' family records.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#submit' => array('ftree_import_submit'),
  );
  return $form;
}
function ftree_import_done_form($form, &$form_state){
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Import Completed....press Done'),
    '#markup' => t('Processed '.$form_state['irecs'].' individual records and '.$form_state['frecs'].' family records.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Done'),
    '#submit' => array('ftree_import_done_submit'),
  );
  return $form;
}
/**
 * Check the uploaded GEDCOM file
  */
function ftree_import_validate($form, &$form_state){
  $file = file_save_upload('gedcom_file', array(
    'file_validate_extensions' => array('ged'),
  ));
  if($file){
    $form_state['gedfile'] = $file->uri; 
    if ($form_state['values']['replace']) {
      db_query("TRUNCATE {ftree_individual}");
      db_query("TRUNCATE {ftree_family}");
    }
  }
  else {
    form_set_error('', t("Didn't get GED file"));
  }
}

function ftree_import_submit($form, &$form_state) {
  $max_time = ini_get('max_execution_time') - 2;
  $start_time = microtime_float();
  $file_offset = $form_state['gedoffset'];
  $file = $form_state['gedfile'];
  $import = new Gedcom();
  $ok = $import->parseGedcom($file, $file_offset,$start_time,$max_time);
  list($offset, $inds, $fams) = $ok;
  $form_state['gedoffset'] = $offset;
  $form_state['irecs'] += $inds;
  $form_state['frecs'] += $fams;
  $form_state['page_num'] = ($offset > 0) ? 2 :3;
  $form_state['rebuild'] = TRUE;
}
function ftree_import_done_submit(){
  drupal_set_message('Import Finished');
}



